package test.parts;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.log.Logger;
import org.eclipse.e4.ui.di.UIEventTopic;

import test.animation.ACenterSwitchAnimation;
import test.animation.PageChangeAnimation;
import test.handlers.BackHandler;
import test.ui.factory.WigetFactory;
import test.ui.page.IPage;
import test.ui.page.KantinenPage;

public class OverviewPart {

	private ACenterSwitchAnimation pageChangeAnimation;
	private TilePane tilePane;
	private BorderPane borderPane;
	
//	@PostConstruct
	@Inject
	public void init(final BorderPane p) {
		pageChangeAnimation = new PageChangeAnimation();
//		pageChangeAnimation = new FadeAnimation();
//		pageChangeAnimation = new FlipAnimation();
//		pageChangeAnimation = new RotateOutAnimation();
//		pageChangeAnimation = new SlideAnimation();
//		pageChangeAnimation = new ZoomSlideAnimation();
		tilePane = new TilePane(16, 16);
		this.borderPane = p;
		Button btKantine = WigetFactory.createTile("Kantine",
				"koch2.jpg");
		createHandler(btKantine, new KantinenPage());

		Button btAnbieter = WigetFactory.createTile("Anbieter",
				"anbieter1.png");
		
		Button btSpeiseplan = WigetFactory.createTile("Speiseplan",
				"speiseplan.jpg");
		
		Button btEinkaufsliste = WigetFactory.createTile("Einkaufsliste",
				"einkauf.png");
		
		Button btGerichte = WigetFactory.createTile("Gerichte",
				"player-volume.png");

		tilePane.getChildren().add(btKantine);
		tilePane.getChildren().add(btAnbieter);
		tilePane.getChildren().add(btSpeiseplan);
		tilePane.getChildren().add(btGerichte);
		tilePane.getChildren().add(btEinkaufsliste);
		p.setCenter(tilePane);
	}

	private void createHandler(Button button, final IPage page) {
		button.setOnAction(new EventHandler() {
			public void handle(Event event) {
				Node newPage = page.createView();
				pageChangeAnimation.animate(borderPane, newPage);
			}
		});
	}

	@Inject
	@Optional
	public void back(@UIEventTopic(BackHandler.BACK_EVENT) String event) {
		if(borderPane.getCenter() != tilePane){
			pageChangeAnimation.animate(borderPane, tilePane);
		}
	}

}