package test.ui.page;

import javafx.scene.Node;

public interface IPage {
	Node createView();
}
