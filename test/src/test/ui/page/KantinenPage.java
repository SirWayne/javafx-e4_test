package test.ui.page;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class KantinenPage implements IPage{

	public Node createView(){
		BorderPane borderPane = new BorderPane();
		Label label = new  Label("Title");
		label.getStyleClass().add("title-label");

		TableView<Object> tableView = new TableView<Object>();
		
		TableColumn<Object, Object> col1 = new TableColumn<Object, Object>();
		col1.setText("Col1");
		
		TableColumn<Object, Object> col2 = new TableColumn<Object, Object>();
		col2.setText("Col2");
		
		TableColumn<Object, Object> col3 = new TableColumn<Object, Object>();
		col3.setText("Col3");
		
		
		tableView.getColumns().add(col1);
		tableView.getColumns().add(col2);
		tableView.getColumns().add(col3);
		
	    HBox hbox = new HBox();
	   hbox.getStyleClass().add("title-label");
	    hbox.setSpacing(10);
	    hbox.getChildren().add(label);
	    
		borderPane.setTop(hbox);
		borderPane.setCenter(tableView);
		return borderPane;
	}
	
}
