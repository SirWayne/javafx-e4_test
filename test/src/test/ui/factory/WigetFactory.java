package test.ui.factory;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

public class WigetFactory {

	private static final String ICON_PATH = "platform:/plugin/test/icons/";

	public static Button createTile(String name, String image) {
		Button tile = new Button(name, getIcon(image));
		tile.setContentDisplay(ContentDisplay.TOP);
		tile.getStyleClass().clear();
		tile.getStyleClass().add("sample-tile");
		return tile;
	}

	private static Node getIcon(String image) {
		ImageView imageView = new ImageView(ICON_PATH + "icon-overlay.png");
		imageView.setMouseTransparent(true);
		Rectangle overlayHighlight = new Rectangle(-8, -8, 130, 130);
		overlayHighlight.setFill(new LinearGradient(0, 0.5, 0, 1, true,
				CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, Color.BLACK),
						new Stop(1, Color.web("#444444")) }));
		overlayHighlight.setOpacity(0.8);
		overlayHighlight.setMouseTransparent(true);
		overlayHighlight.setBlendMode(BlendMode.ADD);
		Rectangle background = new Rectangle(-8, -8, 130, 130);
		background.setFill(Color.web("#b9c0c5"));
		Group group = new Group(background);
		Rectangle clipRect = new Rectangle(114, 114);
		clipRect.setArcWidth(38);
		clipRect.setArcHeight(38);
		group.setClip(clipRect);
		Node content = new ImageView(ICON_PATH + image);
		content.setTranslateX((int) ((114 - content.getBoundsInParent()
				.getWidth()) / 2) - (int) content.getBoundsInParent().getMinX());
		content.setTranslateY((int) ((114 - content.getBoundsInParent()
				.getHeight()) / 2)
				- (int) content.getBoundsInParent().getMinY());
		group.getChildren().add(content);
		group.getChildren().addAll(overlayHighlight, imageView);
		// Wrap in extra group as clip dosn't effect layout without it
		return new Group(group);
	}

}
